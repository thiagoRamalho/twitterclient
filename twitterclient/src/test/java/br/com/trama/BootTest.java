package br.com.trama;

import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.trama.twitterclient.core.SearchFollowers;
import br.com.trama.twitterclient.model.dao.FollowerEntityDAO;
import br.com.trama.twitterclient.model.dao.UserEntityDAO;
import br.com.trama.twitterclient.model.entity.UserEntity;

public class BootTest {

	private static UserEntityDAO userEntityDAO;
	private static FollowerEntityDAO followerEntityDAO;

	public static void main(String[] args) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("teste");
		EntityManager em = factory.createEntityManager();

		em.getTransaction().begin();
		
		userEntityDAO = new UserEntityDAO(em);
		followerEntityDAO = new FollowerEntityDAO(em);
		
		createUserEntity(em);

		SearchFollowers searchFollowers = new SearchFollowers(userEntityDAO, followerEntityDAO);

		searchFollowers.run();
		
		em.close();

		factory.close();
		
		System.exit(0);
	}

	private static void createUserEntity(EntityManager em) {

		followerEntityDAO.deleteAll();

		userEntityDAO.deleteAll();

		UserEntity userEntity1 = new UserEntity();
		userEntity1.setScreenName("TiagoLeifert");
		userEntity1.setDate(Calendar.getInstance());

		UserEntity userEntity2 = new UserEntity();
		userEntity2.setScreenName("thinet");
		userEntity2.setDate(Calendar.getInstance());

		userEntityDAO.save(userEntity1);
		userEntityDAO.save(userEntity2);
	}
}
