package br.com.trama.twitterclient.view.panel;

import br.com.trama.twitterclient.model.entity.UserEntity;


public interface TabSelectable {
	 void reset();
	 void refreshData(UserEntity entity);
}
