package br.com.trama.twitterclient.model.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.trama.twitterclient.model.entity.FollowerEntity;
import br.com.trama.twitterclient.model.entity.UserEntity;

public class FollowerEntityDAO {

	private EntityManager em;

	public FollowerEntityDAO(EntityManager em) {
		super();
		this.em = em;
	}

	public List<FollowerEntity> findAll(){

		String hql = " from FollowerEntity p ORDER BY p.id ASC ";

		TypedQuery<FollowerEntity> query = this.em.createQuery(hql, FollowerEntity.class);

		return query.getResultList();
	}

	public void deleteAll() {

		String hql = " delete from FollowerEntity ";

		Query query = this.em.createQuery(hql);

		query.executeUpdate();
	}

	public void save(FollowerEntity entity) {
		em.merge(entity);
	}

	public long countByUser(UserEntity userEntity) {

		String jpql = "SELECT count(o) FROM FollowerEntity o where o.userEntity.id=:pId";  

		Query query = this.em.createQuery(jpql);

		query.setParameter("pId", userEntity.getId());

		return (Long) query.getSingleResult();

	}

	public List<FollowerEntity> findByScreenName(String name) {

		List<FollowerEntity> entities = new ArrayList<FollowerEntity>();

		try{
			String hql = " from FollowerEntity p WHERE UPPER(p.screenName) like :pName";

			TypedQuery<FollowerEntity> query = this.em.createQuery(hql, FollowerEntity.class);

			query.setParameter("pName", name.trim().toUpperCase());

			entities = query.getResultList();

		}catch(NoResultException e){
			entities = new ArrayList<FollowerEntity>();
		}

		return entities;
	}

	public List<FollowerEntity> findByIdUser(Long idUser) {	

		List<FollowerEntity> entities = null;

		try{

			String hql = " FROM FollowerEntity o where o.userEntity.id=:pIdUser ORDER BY o.screenName ASC";

			TypedQuery<FollowerEntity> query = this.em.createQuery(hql, FollowerEntity.class);

			query.setParameter("pIdUser", idUser);

			entities = query.getResultList();

		}catch(NoResultException e){
			entities = new ArrayList<FollowerEntity>();
		}

		return entities;
	}

	public List<FollowerEntity> findByIdUserWithPagination(Long idUser, int first, int max) {	

		List<FollowerEntity> entities = null;

		try{

			String hql = " FROM FollowerEntity o where o.userEntity.id=:pIdUser ORDER BY o.id ASC";

			TypedQuery<FollowerEntity> query = this.em.createQuery(hql, FollowerEntity.class);

			query.setParameter("pIdUser", idUser);
			query.setFirstResult(first);
			query.setMaxResults(max);
			
			entities = query.getResultList();

		}catch(NoResultException e){
			entities = new ArrayList<FollowerEntity>();
		}

		return entities;
	}

}
