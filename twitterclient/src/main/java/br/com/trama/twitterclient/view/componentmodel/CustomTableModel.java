package br.com.trama.twitterclient.view.componentmodel;

import java.util.Collections;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.com.trama.twitterclient.core.TwitterConstants;

public abstract class CustomTableModel<T> extends AbstractTableModel{

	private static final long serialVersionUID = 1L;

	protected List<T> dados = Collections.emptyList();
	private String[] colunas;
	
	public CustomTableModel(String[] columns, List<T> dados) {
		super();
		if(columns != null){
			this.dados = dados;
		}
		this.colunas = columns;
	}

	/**
	 * Total de linhas, caso vazio seta valor padrão
	 */
	@Override
	public int getRowCount() {
		
		int size = this.dados.size();
		
		return size > TwitterConstants.MIN_ROW ? size : TwitterConstants.MIN_ROW;
	}

	/**
	 * Total de colunas, determinado pelos labels do header
	 */
	@Override
	public int getColumnCount() {
		return this.colunas.length;
	}
	
	/**
	 * Validacao para garantir o acesso ao indice valido
	 * da lista de dados
	 * 
	 * @param index
	 * @return
	 */
	protected boolean isValidRow(int index){
		return (index > -1 && index < this.dados.size());
	}
	
	/**
	 * Validacao para garantir o acesso ao indice valido
	 * a lista de headers
	 * @param index
	 * @return
	 */
	protected boolean isValidColumn(int index){
		return (index > -1 && index < this.colunas.length);
	}
	
	/**
	 * Retorna os valores existentes nos headers conforme
	 * o indice enviado
	 * 
	 */
    @Override
    public String getColumnName(int column) {
        return this.isValidColumn(column) ? colunas[column] : "";
    }
    
    /**
     * Retorna o tipo do dado de uma determinada celula
     * importante pois no caso de um boolean, ira torna-la
     * um checkbox
     * 
     */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		
		if(this.dados.isEmpty()){
			return String.class;
		}
		
		return this.getValueAt(0, columnIndex).getClass();
	}
	
	public void setDados(List<T> dados) {
		this.dados = dados;
		fireTableDataChanged();
	}
	
	public T getEntity(int indexRow){
		
		T t = null;
		
		if(isValidRow(indexRow)){
			t = this.dados.get(indexRow);
		}
				
		return t;
	}
}
