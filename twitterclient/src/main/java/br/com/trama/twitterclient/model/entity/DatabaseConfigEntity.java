package br.com.trama.twitterclient.model.entity;

public class DatabaseConfigEntity {

	private String puKey;
	
	private String fileName;

	public DatabaseConfigEntity(String puKey, String fileName) {
		super();
		this.puKey = puKey;
		this.fileName = fileName;
	}

	public String getPuKey() {
		return puKey;
	}

	public String getFileName() {
		return fileName;
	}
	
	@Override
	public String toString() {
		return getPuKey();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((puKey == null) ? 0 : puKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatabaseConfigEntity other = (DatabaseConfigEntity) obj;
		if (puKey == null) {
			if (other.puKey != null)
				return false;
		} else if (!puKey.equals(other.puKey))
			return false;
		return true;
	}
}
