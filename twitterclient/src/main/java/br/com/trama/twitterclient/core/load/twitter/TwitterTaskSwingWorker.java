package br.com.trama.twitterclient.core.load.twitter;

import java.util.List;

import javax.persistence.EntityManager;
import javax.swing.SwingWorker;

import twitter4j.TwitterFactory;
import br.com.trama.twitterclient.core.JPAUtil;
import br.com.trama.twitterclient.core.StatusProcess;
import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.model.dao.FollowerEntityDAO;
import br.com.trama.twitterclient.model.dao.FriendsEntityDAO;
import br.com.trama.twitterclient.model.dao.UserEntityDAO;
import br.com.trama.twitterclient.model.entity.StatusSearch;
import br.com.trama.twitterclient.view.ObserverProcess;

public class TwitterTaskSwingWorker extends SwingWorker<Void, String> implements ObserverProcess{

	private static final long DELAY_IN_MILLIS = 15 * 60 * 1001;
	private Display display;

	public TwitterTaskSwingWorker(Display display) {
		this.display = display;
	}

	@Override
	protected Void doInBackground() {

		try{
			
			clearStatus();

			StatusProcess statusProcess = StatusProcess.FRIENDS;

			while(!StatusProcess.FINISH.equals(statusProcess) && !StatusProcess.ERROR.equals(statusProcess)){

				switch (statusProcess) {

				case FOLLOWERS:
					statusProcess = runProcessFollowers();
					break;

				case FRIENDS:
					statusProcess = runProcessFriends();
					break;

				case WAIT:
					statusProcess = runDelay();
					break;

				case ERROR:
					publish("Erros na execução dos processos");
					break;
					
				default: 
					break;
				}
			}

		}catch(Exception e){
			publish(e.getMessage());
		}

		return null;
	}

	private StatusProcess runDelay() throws InterruptedException {

		publish("Aguardando delay para próximo processamento");

		this.display.startVisualTimer();

		Thread.sleep(DELAY_IN_MILLIS);

		this.display.stopVisualTimer();
		
		return StatusProcess.FOLLOWERS;
	}

	private StatusProcess runProcessFriends() {
		
		this.display.setTitleProcess("Recuperando Amigos");

		publish("Iniciando processo de recuperacao de amigos");

		EntityManager em = JPAUtil.getEM();

		LoadFriendsProcess loadFriendsProcess = 
		new LoadFriendsProcess(new UserEntityDAO(em), new FriendsEntityDAO(em), new TwitterFactory().getInstance());
		
		loadFriendsProcess.setObserverProcess(this);

		StatusProcess statusProcess = loadFriendsProcess.execute();

		publish("Finalizando processo de recuperacao de amigos");

		JPAUtil.close();

		return statusProcess;
	}

	private StatusProcess runProcessFollowers() {

		this.display.setTitleProcess("Recuperando Seguidores");

		publish("Iniciando processo de recuperacao de seguidores");

		EntityManager emf = JPAUtil.getEM();

		LoadFollowersProcess loadFollowersProcess = 
				new LoadFollowersProcess(new UserEntityDAO(emf), new FollowerEntityDAO(emf), new TwitterFactory().getInstance());
		loadFollowersProcess.setObserverProcess(this);

		StatusProcess statusProcess = loadFollowersProcess.execute();

		publish("Finalizando processo de recuperacao de seguidores");

		JPAUtil.close();

		return statusProcess;
	}

	private void clearStatus() {

		EntityManager em = JPAUtil.getEM();

		new UserEntityDAO(em).updateStatusFollowersAndFriendsWhereComplete(StatusSearch.NOT_EXECUTED);

		JPAUtil.close();
	}

	@Override
	protected void done() {
		super.done();

		if(isCancelled()){
			publish("Processo Cancelado");
		}
		else {
			publish("Processo Finalizado!");
		}

		display.finishProcess();
	}

	@Override
	protected void process(List<String> chunks) {
		super.process(chunks);
		firePropertyChange(TwitterConstants.MESSAGE,null,chunks.get(0));
	}

	@Override
	public void update(String string) {
		publish(string);
	}
}
