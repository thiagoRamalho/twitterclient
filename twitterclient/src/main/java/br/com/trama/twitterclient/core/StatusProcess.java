package br.com.trama.twitterclient.core;

public enum StatusProcess {
	ERROR, FINISH, WAIT, FRIENDS, FOLLOWERS, NEXT_USER, CONTINUE_PROCESS;
}
