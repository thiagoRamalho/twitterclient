package br.com.trama.twitterclient.core;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import twitter4j.PagableResponseList;
import twitter4j.RateLimitStatus;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import br.com.trama.twitterclient.model.dao.FollowerEntityDAO;
import br.com.trama.twitterclient.model.dao.UserEntityDAO;
import br.com.trama.twitterclient.model.entity.FollowerEntity;
import br.com.trama.twitterclient.model.entity.StatusSearch;
import br.com.trama.twitterclient.model.entity.UserEntity;

public class SearchFollowers {

	private Logger logger = Logger.getLogger(SearchFollowers.class);

	private UserEntityDAO userEntityDAO;

	private FollowerEntityDAO followerEntityDAO;

	private Twitter twitter;

	public SearchFollowers(UserEntityDAO userEntityDAO, FollowerEntityDAO followerEntityDAO) {
		super();
		this.userEntityDAO = userEntityDAO;
		this.followerEntityDAO = followerEntityDAO;
	}

	public void run(){

		logger.info("iniciando pesquisa");

		try{

			List<UserEntity> users = userEntityDAO.findAll();

			twitter = new TwitterFactory().getInstance();

			for (UserEntity userEntity : users) {

				logger.info("buscando seguidores para: "+userEntity.getScreenName());

				int responseStatus = searchAndSave(userEntity);

				if(TwitterConstants.STATUS_CODE_OK != responseStatus){
					return;
				}
				
				long followersCount = this.followerEntityDAO.countByUser(userEntity);

				logger.info("Total de: ["+followersCount+"] seguidores para: "+userEntity.getScreenName());
			}

		}catch(Exception e){
			logger.error(e.getMessage());
		}

		logger.info("finalizando pesquisa");
	}

	private int searchAndSave(UserEntity userEntity) throws TwitterException {

		int responseStatusCode = TwitterConstants.STATUS_CODE_OK;
		
		long cursor = userEntity.getLastCursorFollowers();

		try{

			cursor = cursor > -1 ? cursor : -1;

			PagableResponseList<User> followersList;

			logger.info("last cursor ["+cursor+"] para ["+userEntity.getScreenName()+"]");

			if(!StatusSearch.COMPLETE.equals(userEntity.getStatusSearchFollowers())){

				do{

					if(cursor != 0 && !this.isRateRemaining()){
						return TwitterConstants.STATUS_CODE_RATE_LIMITED_v1_1;
					}
					
					followersList = 
					twitter.getFollowersList(userEntity.getScreenName(), cursor, TwitterConstants.COUNT_LIMIT);

					cursor = followersList.getNextCursor();

					userEntity.setLastCursorFollowers(cursor);
					userEntity.setStatusSearchFollowers(StatusSearch.PARCIAL);

					logger.info("actual cursor ["+cursor+"] size ["+followersList.size()+"]");

					saveFollowers(userEntity, followersList);

					logger.info("persistido range de followers");
					
				} while (cursor != 0);

				userEntity.setStatusSearchFollowers(cursor > 0 ? StatusSearch.PARCIAL : StatusSearch.COMPLETE);
				userEntity.setLastCursorFollowers(cursor);
			} 
		}catch (TwitterException e) {
			userEntity.setStatusSearchFollowers(StatusSearch.PARCIAL);
			userEntity.setLastCursorFollowers(cursor);
			logger.error(e.getMessage());
			responseStatusCode = e.getStatusCode();
		}
		finally{
			logger.info("update "+userEntity.toString());
			this.userEntityDAO.save(userEntity);
		}
		
		return responseStatusCode;
	}


	/**
	 * Verify limit request 
	 * 
	 * @return
	 */
	private boolean isRateRemaining() {

		boolean isRateRemaining = false;
		boolean isTimeRemaining = false;

		try {
			
			Map<String, RateLimitStatus> mapRateLimitStatus;
			
			mapRateLimitStatus = twitter.getRateLimitStatus(TwitterConstants.RESOURCE_FAMILY_FOLLOWERS);
			RateLimitStatus rateLimitStatus = mapRateLimitStatus.get(TwitterConstants.RESOURCE_KEY_FOLLOWERS_LIST);

			logger.info("Remaining status request  : "+rateLimitStatus.getRemaining());
			logger.info("Remaining status time secs: "+rateLimitStatus.getSecondsUntilReset());

			//verifica se ainda possui requisicoes disponiveis
			//na janela de tempo permitida pela API do Twitter
			isRateRemaining = rateLimitStatus.getRemaining() > 0;
			
			//colocamos um tempo minimo de seguranca para evitar que 
			//a requisicao retorne exception por estouro na janela de tempo
			//permitida pela API do twitter
			isTimeRemaining = rateLimitStatus.getSecondsUntilReset() > TwitterConstants.MINIMUM_TIME_IN_SECONDS;
			
		} catch (TwitterException e) {
			logger.error(e.getMessage());
		}

		return isRateRemaining && isTimeRemaining;
	}

	private void saveFollowers(UserEntity userEntity, PagableResponseList<User> followersList) {

		for (User user : followersList) {

			FollowerEntity followerEntity = new FollowerEntity();
			followerEntity.setUserEntity(userEntity);
			followerEntity.setScreenName(user.getScreenName());

			this.followerEntityDAO.save(followerEntity);

			System.out.println(user.getScreenName()+";");
			
			logger.debug("salvando follower: "+followerEntity.toString());
		}
	}
}
