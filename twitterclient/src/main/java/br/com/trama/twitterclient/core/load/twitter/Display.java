package br.com.trama.twitterclient.core.load.twitter;

public interface Display {

	void finishProcess();

	void startVisualTimer();
	
	void setTitleProcess(String title);
	
	void stopVisualTimer();
}
