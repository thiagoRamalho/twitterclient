package br.com.trama.twitterclient.model.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class FollowerEntity {

	@Id @GeneratedValue
	private Long id;
	
	@ManyToOne
    @JoinColumn(name = "userentity_id")
	private UserEntity userEntity;
	
	private String screenName;

	@Temporal(TemporalType.DATE)
	private Calendar date;
	
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	
	public String getScreenName() {
		return screenName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}
	
	public UserEntity getUserEntity() {
		return userEntity;
	}
	
	public Calendar getDate() {
		return date;
	}
	
	public void setDate(Calendar date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "FollowerEntity [id=" + id + ", screenName=" + screenName + "]";
	}
}
