package br.com.trama.twitterclient.view.componentmodel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.model.entity.FollowerEntity;

public class FollowersTableModel extends CustomTableModel<FollowerEntity>{

	private static final long serialVersionUID = 1L;
	private SimpleDateFormat simpleDateFormat;

	public FollowersTableModel(String[] columns, List<FollowerEntity> arrayList) {
		super(columns, arrayList);
		simpleDateFormat = TwitterConstants.DATE_FORMAT;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		if(!this.dados.isEmpty() && this.dados.size() > rowIndex){

			FollowerEntity entity = this.dados.get(rowIndex);
			
			if(columnIndex == 0){
				return entity.getId();
			}
			
			if(columnIndex == 1){
				return entity.getScreenName();
			}
			
			if(columnIndex == 2){
				
				Calendar date = entity.getDate();
				
				if(date == null){
					return "";
				}
				
				return simpleDateFormat.format(entity.getDate().getTime());
			}
		}
		return "";
	}
}
