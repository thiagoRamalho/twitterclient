package br.com.trama.twitterclient.view.progress;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import org.joda.time.DateTime;

import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.core.load.twitter.Display;
import br.com.trama.twitterclient.core.load.twitter.TwitterTaskSwingWorker;

public class ProgressTwitterDataDialog extends JDialog implements PropertyChangeListener, Display {

	private static final long serialVersionUID = 1L;

	private JTextArea textAreaLog;

	private JButton btnStop;

	private TwitterTaskSwingWorker taskSwingWorker;
	
	private JLabel lblTimeWaitValue;
	
	private Timer pauseTimer;

	private JLabel lblProgressDescriptionText;

	private JLabel lblPauseTime;

	/**
	 * Create the frame.
	 * @param owner 
	 */
	public ProgressTwitterDataDialog(Frame owner) {
		super(owner);
		
		this.taskSwingWorker = new TwitterTaskSwingWorker(this);
		
		setModal(true);
		setBounds(100, 100, 300, 276);
		setResizable(false);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panelHeader = new JPanel();
		panelHeader.setBounds(6, 10, 288, 49);
		contentPane.add(panelHeader);
		panelHeader.setLayout(null);
		panelHeader.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		
		JLabel lblProcessDescription = new JLabel("Description:");
		lblProcessDescription.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblProcessDescription.setBounds(6, 6, 80, 16);
		panelHeader.add(lblProcessDescription);
		
		lblProgressDescriptionText = new JLabel("Aguarde...");
		lblProgressDescriptionText.setBounds(98, 6, 184, 16);
		panelHeader.add(lblProgressDescriptionText);
		
		JLabel lblStatusProcess = new JLabel("Status:");
		lblStatusProcess.setHorizontalAlignment(SwingConstants.RIGHT);
		lblStatusProcess.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblStatusProcess.setBounds(6, 23, 80, 16);
		panelHeader.add(lblStatusProcess);
		
		JLabel lblStatusProcessDescription = new JLabel("Executando");
		lblStatusProcessDescription.setBounds(98, 23, 184, 16);
		panelHeader.add(lblStatusProcessDescription);
		
		textAreaLog = new JTextArea();
		textAreaLog.setEditable(false);
		textAreaLog.setRows(3);
		textAreaLog.setColumns(20);
		
		JScrollPane scrollPane = new JScrollPane(textAreaLog);
		scrollPane.setBounds(6, 110, 288, 87);
		contentPane.add(scrollPane);
		
		JPanel panelButton = new JPanel();
		panelButton.setBounds(6, 209, 288, 39);
		contentPane.add(panelButton);
		panelButton.setLayout(null);
		
		btnStop = new JButton("Stop");
		btnStop.addActionListener(new CloseAdapter());
		btnStop.setBounds(165, 6, 117, 29);
		btnStop.setEnabled(false);
		panelButton.add(btnStop);
		panelButton.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		
		JPanel panelElapsedTime = new JPanel();
		panelElapsedTime.setBounds(6, 71, 288, 27);
		contentPane.add(panelElapsedTime);
		panelElapsedTime.setLayout(null);
		panelElapsedTime.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		panelElapsedTime.setEnabled(false);

		lblPauseTime = new JLabel("Time Wait:");
		lblPauseTime.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPauseTime.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblPauseTime.setBounds(6, 6, 80, 16);
		lblPauseTime.setEnabled(false);
		panelElapsedTime.add(lblPauseTime);
		
		lblTimeWaitValue = new JLabel("00:00:00");
		lblTimeWaitValue.setBounds(98, 6, 61, 16);
		lblTimeWaitValue.setEnabled(false);
		panelElapsedTime.add(lblTimeWaitValue);

		this.setLocationRelativeTo(null);
	}

	public void start() {
		taskSwingWorker.addPropertyChangeListener(this);
		taskSwingWorker.execute();
	}
	
	private String formatDate(int value){
		return (value < 10 ? "0" : "") + value;
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {        

		if (TwitterConstants.PROGRESS.equals(evt.getPropertyName())) {
			//progressBar.setValue(progress);
			textAreaLog.append(String.format(
					"Completed %d%% of task.\n", evt.getNewValue()));
		} 
		else if(TwitterConstants.MESSAGE.equals(evt.getPropertyName())){
			textAreaLog.append(String.format("%s \n", evt.getNewValue()));
		}
	}

	
	class MyTimerActionListener implements ActionListener{
		
		private DateTime timer = new DateTime(2014, 1, 1, 1, 15, 0);
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			timer = timer.minusSeconds(1);
			
			int minuteOfHour = timer.getMinuteOfHour();
			int secondOfMinute = timer.getSecondOfMinute();
			
			if(minuteOfHour < 1 && secondOfMinute < 1){
				lblTimeWaitValue.setText("00:00:00");
				pauseTimer.stop();
				return;
			}
			
			lblTimeWaitValue.setText("00:"+formatDate(minuteOfHour)+":"+formatDate(secondOfMinute));
		}
	}

	@Override
	public void finishProcess() {
		ProgressTwitterDataDialog.this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		btnStop.setEnabled(true);
	}

	@Override
	public void startVisualTimer() {
		
		stopVisualTimer();
		
		lblTimeWaitValue.setEnabled(true);
		lblPauseTime.setEnabled(true);
		
		pauseTimer = new Timer(1000, new MyTimerActionListener());
		
		setTitleProcess("Aguardando Delay...");
		
		pauseTimer.start();
	}
	
	@Override
	public void stopVisualTimer() {
		
		if(pauseTimer!= null && pauseTimer.isRunning()){
			pauseTimer.stop();
		}
		
		lblTimeWaitValue.setEnabled(false);
		lblPauseTime.setEnabled(false);
		
		lblTimeWaitValue.setText("00:00:00");
	}
	

	@Override
	public void setTitleProcess(String title) {
		this.lblProgressDescriptionText.setText(title);
	}
	
	class CloseAdapter extends WindowAdapter implements ActionListener{
		
		@Override
		public void windowClosed(WindowEvent e) {
			close();
		}

		private void close() {
			ProgressTwitterDataDialog.this.dispose();
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			close();
		}
	}
}
