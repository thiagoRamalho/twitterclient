package br.com.trama.twitterclient.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;

public class SwingUtil {

	public static final String[] headerTableUsers  = {"ID", "SCREEN NAME", "STATUS FRIENDS", "STATUS FOLLOWERS", "DATE UPDATE"};
	public static final String[] headerTableOthers = {"ID", "SCREEN NAME", "DATE UPDATE"};

	public static JTable createTable(TableModel tableModel){

		JTable table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setAlignmentY(Component.TOP_ALIGNMENT);
		table.setAlignmentX(Component.LEFT_ALIGNMENT);
		table.setGridColor(Color.LIGHT_GRAY);
		
		return table;
	
	}
	
	public static JScrollPane createJScrollPane(JTable jTable) {
		JScrollPane scrollPane = new JScrollPane(jTable);
		scrollPane.setBounds(6, 49, 595, 210);
		//scrollPane.setBackground(this.getBackground());
		return scrollPane;
	}

}
