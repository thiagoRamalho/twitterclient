package br.com.trama.twitterclient.core;

import br.com.trama.twitterclient.view.ObserverProcess;

public interface SubjectProcess {

	StatusProcess execute();

	void setObserverProcess(ObserverProcess observerProcess);
}
