package br.com.trama.twitterclient.core.csv.write;

import br.com.trama.twitterclient.core.StatusProcess;
import br.com.trama.twitterclient.core.SubjectProcess;
import br.com.trama.twitterclient.view.ObserverProcess;

public abstract class WriteCSVFileProcess implements SubjectProcess{

	protected ObserverProcess observerProcess;

	@Override
	public StatusProcess execute() {
		
		doProcess();
		
		return StatusProcess.FINISH;
	}

	@Override
	public void setObserverProcess(ObserverProcess observerProcess) {
		this.observerProcess = observerProcess;
	}
	
	protected abstract void doProcess();
}
