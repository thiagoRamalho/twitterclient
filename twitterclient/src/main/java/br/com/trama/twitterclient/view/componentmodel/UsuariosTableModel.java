package br.com.trama.twitterclient.view.componentmodel;

import java.text.SimpleDateFormat;
import java.util.List;

import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.model.entity.UserEntity;

public class UsuariosTableModel extends CustomTableModel<UserEntity>{

	private static final long serialVersionUID = 1L;
	private SimpleDateFormat simpleDateFormat;

	public UsuariosTableModel(String[] columns, List<UserEntity> arrayList) {
		super(columns, arrayList);
		simpleDateFormat = TwitterConstants.DATE_FORMAT;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		if(!this.dados.isEmpty() && this.isValidRow(rowIndex)){

			UserEntity userEntity = this.dados.get(rowIndex);
			
			if(columnIndex == 0){
				return userEntity.getId();
			}
			
			if(columnIndex == 1){
				return userEntity.getScreenName();
			}
			
			if(columnIndex == 2){
				return userEntity.getStatusSearchFriends();
			}
			
			if(columnIndex == 3){
				return userEntity.getStatusSearchFollowers();
			}
			
			if(columnIndex == 4){
				return simpleDateFormat.format(userEntity.getDate().getTime());
			}
		}
		return "";
	}
}
