package br.com.trama.twitterclient.model.entity;

public enum StatusSearch {
	PARCIAL, COMPLETE, NOT_EXECUTED, NOT_FOUND; 
}
