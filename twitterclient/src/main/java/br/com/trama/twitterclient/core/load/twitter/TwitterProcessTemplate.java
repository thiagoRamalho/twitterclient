package br.com.trama.twitterclient.core.load.twitter;

import java.util.Map;

import org.apache.log4j.Logger;

import twitter4j.RateLimitStatus;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import br.com.trama.twitterclient.core.SubjectProcess;
import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.view.ObserverProcess;

public abstract class TwitterProcessTemplate implements SubjectProcess{

	private Logger logger = Logger.getLogger(TwitterProcessTemplate.class);

	protected Twitter twitter;

	protected ObserverProcess observerProcess;

	public TwitterProcessTemplate(Twitter twitter) {
		super();
		this.twitter = twitter;
	}

	protected boolean isRateLimiteStatus(int responseStatus) {
		return TwitterConstants.STATUS_CODE_RATE_LIMITED_v1_0 == responseStatus || 
				TwitterConstants.STATUS_CODE_RATE_LIMITED_v1_1 == responseStatus;
	}

	/**
	 * Verify limit request 
	 * 
	 * @return
	 */
	protected boolean isRateRemaining(String rateCode, String resourceKeyFollowersList) {

		boolean isRateRemaining = false;
		boolean isTimeRemaining = false;

		try {

			Map<String, RateLimitStatus> mapRateLimitStatus;

			mapRateLimitStatus = twitter.getRateLimitStatus(rateCode);
			RateLimitStatus rateLimitStatus = mapRateLimitStatus.get(resourceKeyFollowersList);

			//verifica se ainda possui requisicoes disponiveis
			//na janela de tempo permitida pela API do Twitter
			isRateRemaining = rateLimitStatus.getRemaining() > 0;

			//colocamos um tempo minimo de seguranca para evitar que 
			//a requisicao retorne exception por estouro na janela de tempo
			//permitida pela API do twitter
			isTimeRemaining = rateLimitStatus.getSecondsUntilReset() > TwitterConstants.MINIMUM_TIME_IN_SECONDS;

		} catch (TwitterException e) {
			logger.error(e.getMessage());
		}

		return isRateRemaining && isTimeRemaining;
	}

	protected void publish(String msg) {		
		if(this.observerProcess != null){
			this.observerProcess.update(msg);
		}
	}

	@Override
	public void setObserverProcess(ObserverProcess observerProcess) {
		this.observerProcess = observerProcess;
	}

}
