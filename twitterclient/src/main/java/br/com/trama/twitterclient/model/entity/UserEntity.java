package br.com.trama.twitterclient.model.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class UserEntity {

	@Id @GeneratedValue
	private Long id;
	
	private String screenName;

	@OneToMany(mappedBy = "userEntity")
    private List<FollowerEntity> followers;

	@OneToMany(mappedBy = "userEntity")
    private List<FriendsEntity> friends;

	private long lastCursorFollowers = -1;
	
	private long lastCursorFriends = -1;
	
	@Temporal(TemporalType.DATE)
	private Calendar date;
	
	@Enumerated(EnumType.STRING)
	private StatusSearch statusSearchFollowers;

	@Enumerated(EnumType.STRING)
	private StatusSearch statusSearchFriends;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public long getLastCursorFollowers() {
		return lastCursorFollowers;
	}

	public void setLastCursorFollowers(long lastCursorFollowers) {
		this.lastCursorFollowers = lastCursorFollowers;
	}

	public long getLastCursorFriends() {
		return lastCursorFriends;
	}

	public void setLastCursorFriends(long lastCursorFriends) {
		this.lastCursorFriends = lastCursorFriends;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public void setStatusSearchFollowers(StatusSearch statusSearchFollowers) {
		this.statusSearchFollowers = statusSearchFollowers;
	}
	
	public StatusSearch getStatusSearchFollowers() {
		return statusSearchFollowers;
	}
	
	public void setStatusSearchFriends(StatusSearch statusSearchFriends) {
		this.statusSearchFriends = statusSearchFriends;
	}
	
	public StatusSearch getStatusSearchFriends() {
		return statusSearchFriends;
	}
	
	public List<FollowerEntity> getFollowers() {
		return followers;
	}

	public List<FriendsEntity> getFriends() {
		return friends;
	}

	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", screenName=" + screenName + "]";
	}
}
