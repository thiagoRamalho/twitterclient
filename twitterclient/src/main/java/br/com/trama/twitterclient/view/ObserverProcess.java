package br.com.trama.twitterclient.view;

public interface ObserverProcess {

	void update(String string);
	
	boolean isCancelled();
	
}
