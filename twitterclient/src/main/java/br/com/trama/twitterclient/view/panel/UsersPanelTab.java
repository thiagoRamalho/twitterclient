package br.com.trama.twitterclient.view.panel;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTable;

import br.com.trama.twitterclient.core.JPAUtil;
import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.core.csv.read.ReadUsersCSVProcess;
import br.com.trama.twitterclient.model.dao.UserEntityDAO;
import br.com.trama.twitterclient.model.entity.UserEntity;
import br.com.trama.twitterclient.view.FileChooserUtil;
import br.com.trama.twitterclient.view.SwingUtil;
import br.com.trama.twitterclient.view.componentmodel.UsuariosTableModel;
import br.com.trama.twitterclient.view.progress.ProgressDialog;
import br.com.trama.twitterclient.view.progress.ProgressTwitterDataDialog;

public class UsersPanelTab extends JPanel implements TabSelectable{

	private static final long serialVersionUID = 1L;
	
	private Frame frame;
	private JTable table;
	private UsuariosTableModel tableModel;

	private JButton buttonLast;

	private JButton buttonNext;

	private JButton buttonPrev;

	private int pageNumber = 0;

	private long countRows;

	private JButton buttonFirst;

	public UsersPanelTab(Frame frame) {

		this.frame = frame;
		
		this.setLayout(null);

		JPanel internPanel = new JPanel();
		internPanel.setBounds(6, 320, 595, 38);
		this.add(internPanel);
		internPanel.setLayout(null);
		internPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		
		JButton btnImportCSV = new JButton("Importar Usuários CSV");
		btnImportCSV.setBounds(6, 6, 150, 29);
		btnImportCSV.addActionListener(new ImportUserCSV());
		internPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		internPanel.add(btnImportCSV);
		
		JButton btnLoadTwitterData = new JButton("Recuperar Dados Twitter");
		btnLoadTwitterData.addActionListener(new LoadTwitterDataListener());
		btnLoadTwitterData.setBounds(180, 6, 150, 29);
		internPanel.add(btnLoadTwitterData);
		
		tableModel = new UsuariosTableModel(SwingUtil.headerTableUsers, new ArrayList<UserEntity>());
		this.table = SwingUtil.createTable(tableModel);
		this.add(SwingUtil.createJScrollPane(table));
		
		JPanel panelPagination = new JPanel();
		panelPagination.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		panelPagination.setBounds(6, 270, 595, 38);
		add(panelPagination);
		panelPagination.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		buttonFirst = new JButton("<<");
		buttonFirst.addActionListener(new FirstPaginationListener());
		panelPagination.add(buttonFirst);
		
		buttonPrev = new JButton("<");
		buttonPrev.addActionListener(new BackPaginationListener());
		panelPagination.add(buttonPrev);
		
		buttonNext = new JButton(">");
		buttonNext.addActionListener(new NextPaginationListener());
		panelPagination.add(buttonNext);
		
		buttonLast = new JButton(">>");
		buttonLast.addActionListener(new LastPaginationListener());
		panelPagination.add(buttonLast);
		
		reset();
	}

	public void reset() {

		pageNumber = 0;

		getMaxRowsCount();

		this.refreshData(null);
		
		this.configButtons();
	}

	private void getMaxRowsCount() {
		
		EntityManager em = JPAUtil.getEM();
		
		UserEntityDAO userEntityDAO = new UserEntityDAO(em);
		
		countRows = userEntityDAO.count();
		
		JPAUtil.close();
	}

	public void refreshData(UserEntity entity) {
		
		UserEntityDAO userEntityDAO = new UserEntityDAO(JPAUtil.getEM());
		
		List<UserEntity> list = userEntityDAO.findWithPagination(pageNumber, TwitterConstants.MIN_ROW);
		
		JPAUtil.close();
		
		this.tableModel.setDados(list);

		this.table.setEnabled(!list.isEmpty());
		
		if(!list.isEmpty()){
			this.table.setRowSelectionInterval(0, 0);
		}
	}
	
	public UserEntity getEntitySelected() {
		
		int selectedRow = this.table.getSelectedRow();
		
		UserEntity entity = null;
		
		if(selectedRow > -1){
			entity = this.tableModel.getEntity(selectedRow);
		}
		
		return entity;
	}

	private void configButtons() {
		
		boolean isNext = countRows > (pageNumber + TwitterConstants.MIN_ROW);
		boolean isBack = pageNumber > 0;

		buttonFirst.setEnabled(isBack);
		buttonPrev.setEnabled(isBack);
		buttonNext.setEnabled(isNext);
		buttonLast.setEnabled(isNext);
	}
	
	class LastPaginationListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			int lastPageNumber = (int) (countRows / TwitterConstants.MIN_ROW);
			
			pageNumber = lastPageNumber * TwitterConstants.MIN_ROW;
			
			if(countRows > pageNumber){
				refreshData(null);
			}
			
			configButtons();
		}
	}


	class NextPaginationListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			pageNumber+= TwitterConstants.MIN_ROW;
			
			if(countRows > pageNumber){
				refreshData(null);
			}
			
			configButtons();
		}
	}
	
	class BackPaginationListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			pageNumber-= TwitterConstants.MIN_ROW;
			
			pageNumber = pageNumber > -1 ? pageNumber : 0;
			
			if(countRows > pageNumber){
				refreshData(null);
			}
			
			configButtons();
		}
	}
	class FirstPaginationListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			pageNumber = 0;
			
			if(countRows > pageNumber){
				refreshData(null);
			}
			
			configButtons();
		}
	}
	
	class ImportUserCSV implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			if (FileChooserUtil.open(frame) == JFileChooser.APPROVE_OPTION) {
				
				File selectedFile = FileChooserUtil.getSelectedFile();
				
				ReadUsersCSVProcess readUsersSwingWork = new ReadUsersCSVProcess(selectedFile);
				ProgressDialog progressDialog = 
				new ProgressDialog(frame, readUsersSwingWork, "Importando Usuários");
				progressDialog.start();
				progressDialog.setVisible(true);
			
				reset();
			} 
		}
	}
	
	class LoadTwitterDataListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			ProgressTwitterDataDialog progressTwitterDataDialog = 
			new ProgressTwitterDataDialog(frame);
			progressTwitterDataDialog.start();
			progressTwitterDataDialog.setVisible(true);
			
			//quando finalizar a tela ira atualizar o grid
			reset();
		}
	}
}
