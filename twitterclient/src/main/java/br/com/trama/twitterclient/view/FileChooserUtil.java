package br.com.trama.twitterclient.view;

import java.awt.Component;
import java.io.File;

import javax.swing.filechooser.FileFilter;
import javax.swing.JFileChooser;

public abstract class FileChooserUtil {
	
	private static JFileChooser fileChooser;
	
	static{
		fileChooser = new JFileChooser(System.getProperty("user.home"));
	}
	
	public static int open(Component component){
		
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setFileFilter(new CustomFileFilter());
		fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
		
		return fileChooser.showOpenDialog(component);
	}
	
	public static int save(Component component){
		fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
		return fileChooser.showSaveDialog(component);
	}
	
	static class CustomFileFilter extends FileFilter{

		@Override
		public String getDescription() {
			return "*.txt, *.csv";
		}

		@Override
		public boolean accept(File file) {

			String name = file.getName().toUpperCase();

			return name.endsWith(".TXT") || name.endsWith(".CSV");
		}
	}

	public static File getSelectedFile() {
		return fileChooser.getSelectedFile();
	}
}
