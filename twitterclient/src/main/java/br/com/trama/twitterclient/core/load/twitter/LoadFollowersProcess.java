package br.com.trama.twitterclient.core.load.twitter;

import java.util.Calendar;
import java.util.List;

import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import br.com.trama.twitterclient.core.StatusProcess;
import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.model.dao.FollowerEntityDAO;
import br.com.trama.twitterclient.model.dao.UserEntityDAO;
import br.com.trama.twitterclient.model.entity.FollowerEntity;
import br.com.trama.twitterclient.model.entity.StatusSearch;
import br.com.trama.twitterclient.model.entity.UserEntity;

public class LoadFollowersProcess extends TwitterProcessTemplate{

	private UserEntityDAO userEntityDAO;

	private FollowerEntityDAO followerEntityDAO;

	private StringBuilder builder;

	public LoadFollowersProcess(UserEntityDAO userEntityDAO, FollowerEntityDAO followerEntityDAO, Twitter twitter){
		super(twitter);
		this.userEntityDAO = userEntityDAO;
		this.followerEntityDAO = followerEntityDAO;
	}

	@Override
	public StatusProcess execute() {

		StatusProcess statusProcess = StatusProcess.FRIENDS;
		
		publish("iniciando pesquisa de Seguidores");

		try{
			
			List<UserEntity> users = userEntityDAO.findWithStatusFollower(StatusSearch.NOT_EXECUTED, StatusSearch.PARCIAL);

			for (UserEntity userEntity : users) {
				
				logAppend("buscando seguidores para: "+userEntity.getScreenName());
				
				int responseStatus = searchAndSave(userEntity);

				if(isRateLimiteStatus(responseStatus)){
					
					logAppend("Limite de requisições excedido");
					
					publish(builder.toString());
					
					return StatusProcess.WAIT;
				}
				
				long followersCount = this.followerEntityDAO.countByUser(userEntity);
				
				logAppend("Total de: ["+followersCount+"] seguidores gravados para: "+userEntity.getScreenName());
				
				publish(builder.toString());
				
				builder = null;
			}

		}catch(Exception e){
			statusProcess = StatusProcess.ERROR;
			logAppend(e.getMessage());
			publish(builder.toString());
		}
		finally{
			builder = null;
		}

		return statusProcess;
	}

	private void logAppend(String msg) {
		
		if(builder == null){
			builder = new StringBuilder();
		}
		
		builder.append(TwitterConstants.LINE_SEPARATOR).append(msg);
	}
	
	private int searchAndSave(UserEntity userEntity) throws TwitterException {

		int responseStatusCode = TwitterConstants.STATUS_CODE_OK;
		
		long cursor = userEntity.getLastCursorFollowers();

		try{

			cursor = cursor > -1 ? cursor : -1;

			PagableResponseList<User> followersList;

			logAppend("last cursor ["+cursor+"]");

			if(!StatusSearch.COMPLETE.equals(userEntity.getStatusSearchFollowers())){

				do{

					boolean rateRemaining = this.isRateRemaining(TwitterConstants.RESOURCE_FAMILY_FOLLOWERS, 
							                                     TwitterConstants.RESOURCE_KEY_FOLLOWERS_LIST);
					
					if(cursor != 0 && !rateRemaining){
						return TwitterConstants.STATUS_CODE_RATE_LIMITED_v1_1;
					}
					
					followersList = 
					twitter.getFollowersList(userEntity.getScreenName(), cursor, TwitterConstants.COUNT_LIMIT);

					cursor = followersList.getNextCursor();
					
					userEntity.setLastCursorFollowers(cursor);
					userEntity.setStatusSearchFollowers(StatusSearch.PARCIAL);
					
					StatusProcess statusProcess = saveFollowers(userEntity, followersList);
					
					//se achou algum resultado significa que ja recuperou todos os 
					//registros, para o processo portanto
					if(StatusProcess.NEXT_USER.equals(statusProcess)){
						userEntity.setStatusSearchFollowers(StatusSearch.COMPLETE);
						userEntity.setLastCursorFollowers(0);
						return TwitterConstants.STATUS_CODE_OK;
					}
					
				} while (cursor != 0);
				
				userEntity.setLastCursorFollowers(0);
				userEntity.setStatusSearchFollowers(StatusSearch.COMPLETE);
			} 
			
		}catch (TwitterException e) {

			responseStatusCode = e.getStatusCode();
			userEntity.setStatusSearchFollowers(StatusSearch.PARCIAL);
			userEntity.setLastCursorFollowers(cursor);
			logAppend(String.format(TwitterConstants.ERROR_TWITTER_MSG, e.getErrorCode(), e.getStatusCode(), e.getErrorMessage()));
			
			if(e.getStatusCode() == TwitterConstants.STATUS_CODE_NOT_FOUND){
				userEntity.setStatusSearchFollowers(StatusSearch.NOT_FOUND);
			}
		}
		finally{
			this.userEntityDAO.save(userEntity);
		}
		
		return responseStatusCode;
	}
	
	private StatusProcess saveFollowers(UserEntity userEntity, PagableResponseList<User> followersList) {

		for (User searchFollowerUser : followersList) {

			List<FollowerEntity> entities = followerEntityDAO.findByScreenName(searchFollowerUser.getScreenName());

			if(!entities.isEmpty()){
				return StatusProcess.NEXT_USER;
			}
			
			FollowerEntity followerEntity = new FollowerEntity();
			followerEntity.setUserEntity(userEntity);
			followerEntity.setScreenName(searchFollowerUser.getScreenName());
			followerEntity.setDate(Calendar.getInstance());

			this.followerEntityDAO.save(followerEntity);
		}
		
		return StatusProcess.CONTINUE_PROCESS;
	}
}
