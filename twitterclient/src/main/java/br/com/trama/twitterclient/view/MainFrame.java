package br.com.trama.twitterclient.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import br.com.trama.twitterclient.core.JPAUtil;
import br.com.trama.twitterclient.util.DbConstants;
import br.com.trama.twitterclient.view.dialog.DataBaseDialog;
import br.com.trama.twitterclient.view.panel.FollowersPanelTab;
import br.com.trama.twitterclient.view.panel.FriendsPanelTab;
import br.com.trama.twitterclient.view.panel.TabSelectable;
import br.com.trama.twitterclient.view.panel.UsersPanelTab;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTabbedPane tabbedPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	
            	JPAUtil.getEM();
            	JPAUtil.close();
            	
				MainFrame frame = new MainFrame();
				frame.setVisible(true);
           }
        });
    }
		

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		
		setLocationRelativeTo(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(6, 6, 132, 22);
		menuBar.setBackground(getBackground());
		
		contentPane.add(menuBar);
		
		JMenu jMenu = new JMenu("Configurações");
		menuBar.add(jMenu);
		
		JMenuItem jMenuItemDatabBase = new JMenuItem("Banco de Dados");
		jMenuItemDatabBase.addActionListener(new DataBaseActionListener());
		jMenu.add(jMenuItemDatabBase);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(6, 42, 628, 410);
		contentPane.add(tabbedPane);

		JPanel panelUsers = new UsersPanelTab(this);
		tabbedPane.addTab("Usuários", null, panelUsers, null);

		JPanel panelFriends = new FriendsPanelTab(this);
		tabbedPane.addTab("Amigos", null, panelFriends, null);

		JPanel panelFollowers = new FollowersPanelTab(this);
		tabbedPane.addTab("Seguidores", null, panelFollowers, null);
		
		tabbedPane.addChangeListener(new SelectedTabListener());
	}

	@Override
	public void setVisible(boolean b) {
		super.setVisible(b);
		verifyDB();
	}
	
	private void verifyDB() {
		if(JPAUtil.getFinalPU().equals(DbConstants.HSQLDB_PU)){
			
			String message = "Utilizando Banco de Dados em Memória, "
					+ "\nquaisquer alterações poderão ser perdidas "
					+ "\napós finalizar a aplicação!"
					+ "\nAltere a conexão no menu configurações!";
			
			JOptionPane.showMessageDialog(this, message, "Atenção", JOptionPane.WARNING_MESSAGE);
		}
	}

	class SelectedTabListener implements ChangeListener{

		@Override
		public void stateChanged(ChangeEvent e) {
			
			 JTabbedPane sourceTabbedPane = (JTabbedPane) e.getSource();
		     int index = sourceTabbedPane.getSelectedIndex();
		     
		     UsersPanelTab userTab = (UsersPanelTab) sourceTabbedPane.getComponent(0);
		     
		     if(index > 0){
		    	 TabSelectable tabComponentAt = (TabSelectable) tabbedPane.getComponentAt(index);
		    	 tabComponentAt.reset();
		    	 tabComponentAt.refreshData(userTab.getEntitySelected());
		     }
		}
	}
	
	public class DataBaseActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			DataBaseDialog dataBaseDialog = new DataBaseDialog(MainFrame.this);
			dataBaseDialog.setLocationRelativeTo(MainFrame.this);
			dataBaseDialog.setVisible(true);
		}

	}
}
