package br.com.trama.twitterclient.view.componentmodel;

import java.text.SimpleDateFormat;
import java.util.List;

import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.model.entity.FriendsEntity;

public class FriendsTableModel extends CustomTableModel<FriendsEntity>{

	private static final long serialVersionUID = 1L;
	private SimpleDateFormat simpleDateFormat;

	public FriendsTableModel(String[] columns, List<FriendsEntity> arrayList) {
		super(columns, arrayList);
		simpleDateFormat = TwitterConstants.DATE_FORMAT;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		if(!this.dados.isEmpty()){

			FriendsEntity entity = this.dados.get(rowIndex);
			
			if(columnIndex == 0){
				return entity.getId();
			}
			
			if(columnIndex == 1){
				return entity.getScreenName();
			}
			
			if(columnIndex == 2){
				return simpleDateFormat.format(entity.getDate().getTime());
			}
		}
		return "";
	}
}
