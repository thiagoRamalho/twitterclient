package br.com.trama.twitterclient.view.panel;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import br.com.trama.twitterclient.core.JPAUtil;
import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.core.csv.write.WriteCSVFileProcess;
import br.com.trama.twitterclient.core.csv.write.WriteDataFollowersUserCSVFileProcess;
import br.com.trama.twitterclient.model.dao.FollowerEntityDAO;
import br.com.trama.twitterclient.model.entity.FollowerEntity;
import br.com.trama.twitterclient.model.entity.UserEntity;
import br.com.trama.twitterclient.view.FileChooserUtil;
import br.com.trama.twitterclient.view.SwingUtil;
import br.com.trama.twitterclient.view.componentmodel.FollowersTableModel;
import br.com.trama.twitterclient.view.progress.ProgressDialog;

public class FollowersPanelTab extends JPanel implements TabSelectable{

	private static final long serialVersionUID = 1L;

	private JTable table;
	private FollowersTableModel tableModel;

	private JLabel lblSelectedUserValue;

	private int pageNumber = 0;

	private long countRows;

	private AbstractButton buttonFirst;

	private JButton buttonPrev;

	private JButton buttonNext;

	private JButton buttonLast;

	private UserEntity entity;

	private JButton btnExportCSV;

	private Frame frame;

	public FollowersPanelTab(Frame frame) {

		this.frame = frame;
		
		this.setLayout(null);

		JPanel internPanel = new JPanel();
		internPanel.setBounds(6, 320, 595, 38);
		this.add(internPanel);
		internPanel.setLayout(null);
		internPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

		JLabel lblSelectedUser = new JLabel("Usuário Selecionado:");
		lblSelectedUser.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSelectedUser.setBounds(6, 19, 150, 16);
		this.add(lblSelectedUser);

		lblSelectedUserValue = new JLabel("");
		lblSelectedUserValue.setBounds(168, 19, 286, 16);
		lblSelectedUserValue.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		this.add(lblSelectedUserValue);

		btnExportCSV = new JButton("Exportar Seguidores");
		btnExportCSV.setBounds(6, 6, 150, 29);
		btnExportCSV.addActionListener(new ExportDataFollowersCSV());
		internPanel.add(btnExportCSV);

		tableModel = 
				new FollowersTableModel(SwingUtil.headerTableOthers, new ArrayList<FollowerEntity>());

		this.table = SwingUtil.createTable(tableModel);
		this.add(SwingUtil.createJScrollPane(table));

		JPanel panelPagination = new JPanel();
		panelPagination.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		panelPagination.setBounds(6, 270, 595, 38);
		add(panelPagination);
		panelPagination.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		buttonFirst = new JButton("<<");
		buttonFirst.addActionListener(new FirstPaginationListener());
		panelPagination.add(buttonFirst);

		buttonPrev = new JButton("<");
		buttonPrev.addActionListener(new BackPaginationListener());
		panelPagination.add(buttonPrev);

		buttonNext = new JButton(">");
		buttonNext.addActionListener(new NextPaginationListener());
		panelPagination.add(buttonNext);

		buttonLast = new JButton(">>");
		buttonLast.addActionListener(new LastPaginationListener());
		panelPagination.add(buttonLast);
	}		

	public void refreshData(UserEntity entity) {

		this.entity = entity;

		if(this.entity != null){
			lblSelectedUserValue.setText(entity.getScreenName());

			EntityManager em = JPAUtil.getEM();

			FollowerEntityDAO followerEntityDAO = new FollowerEntityDAO(em);

			countRows = followerEntityDAO.countByUser(entity);

			List<FollowerEntity> findByIdUser = 
					followerEntityDAO.findByIdUserWithPagination(entity.getId(), pageNumber, TwitterConstants.MIN_ROW);

			JPAUtil.close();

			populateTable(findByIdUser);
		}
		
		configButtons();
	}

	private void populateTable(List<FollowerEntity> findByIdUser) {
		this.tableModel.setDados(findByIdUser);

		this.table.setEnabled(!findByIdUser.isEmpty());

		if(!findByIdUser.isEmpty()){
			this.table.setRowSelectionInterval(0, 0);
		}
	}

	public FollowerEntity getEntitySelected() {

		int selectedRow = this.table.getSelectedRow();

		FollowerEntity entity = null;

		if(selectedRow > -1){
			entity = this.tableModel.getEntity(selectedRow);
		}

		return entity;
	}

	@Override
	public void reset() {
		pageNumber = 0;
		this.refreshData(entity);
		configButtons();
	}


	private void configButtons() {

		boolean isNext = countRows > (pageNumber + TwitterConstants.MIN_ROW);
		boolean isBack = pageNumber > 0;

		buttonFirst.setEnabled(isBack);
		buttonPrev.setEnabled(isBack);
		buttonNext.setEnabled(isNext);
		buttonLast.setEnabled(isNext);
	}

	class LastPaginationListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			int lastPageNumber = (int) (countRows / TwitterConstants.MIN_ROW) + 1;

			pageNumber = (lastPageNumber - 1) * TwitterConstants.MIN_ROW;

			if(countRows > pageNumber){
				refreshData(entity);
			}

			configButtons();
		}
	}


	class NextPaginationListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			pageNumber+= TwitterConstants.MIN_ROW;

			if(countRows > pageNumber){
				refreshData(entity);
			}

			configButtons();
		}
	}

	class BackPaginationListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			pageNumber-= TwitterConstants.MIN_ROW;

			pageNumber = pageNumber > -1 ? pageNumber : 0;

			if(countRows > pageNumber){
				refreshData(entity);
			}

			configButtons();
		}
	}

	class FirstPaginationListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			pageNumber = 0;

			if(countRows > pageNumber){
				refreshData(entity);
			}

			configButtons();
		}
	}


	class ExportDataFollowersCSV implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			if (FileChooserUtil.save(frame) == JFileChooser.APPROVE_OPTION) {
				
				File selectedFile = FileChooserUtil.getSelectedFile();
				
				WriteCSVFileProcess writeUsersSwingWork = new WriteDataFollowersUserCSVFileProcess(selectedFile);
				
				ProgressDialog progressDialog = 
				new ProgressDialog(frame, writeUsersSwingWork, "Exportando Seguidores");
				
				progressDialog.start();
				progressDialog.setVisible(true);
			} 
		}
	}
}
