package br.com.trama.twitterclient.model.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.trama.twitterclient.model.entity.FriendsEntity;
import br.com.trama.twitterclient.model.entity.UserEntity;

public class FriendsEntityDAO {

	private EntityManager em;
	
	public FriendsEntityDAO(EntityManager em) {
		super();
		this.em = em;
	}

	public void save(FriendsEntity entity) {
		em.merge(entity);
	}

	public long countByUser(UserEntity userEntity) {

		String jpql = "SELECT count(o) FROM FriendsEntity o where o.userEntity.id=:pId";  
		
		Query query = this.em.createQuery(jpql);
		
		query.setParameter("pId", userEntity.getId());
		
		return (Long) query.getSingleResult();
	}
	
	public List<FriendsEntity> findByScreenName(String name) {
		
		List<FriendsEntity> entities = null;
		
		try{
			
			String hql = " from FriendsEntity p WHERE UPPER(p.screenName) like :pName";

			TypedQuery<FriendsEntity> query = this.em.createQuery(hql, FriendsEntity.class);

			query.setParameter("pName", name.trim().toUpperCase());

			entities = query.getResultList();

		}catch(NoResultException e){
			entities = new ArrayList<FriendsEntity>();
		}
		
		return entities;
	}

	public List<FriendsEntity> findByIdUser(Long idUser) {
		
		List<FriendsEntity> entities = null;
		
		try{
			
			String hql = " FROM FriendsEntity o where o.userEntity.id=:pIdUser ORDER BY o.screenName ASC";

			TypedQuery<FriendsEntity> query = this.em.createQuery(hql, FriendsEntity.class);

			query.setParameter("pIdUser", idUser);

			entities = query.getResultList();

		}catch(NoResultException e){
			entities = new ArrayList<FriendsEntity>();
		}
		
		return entities;
	}
	
	public List<FriendsEntity> findByIdUserWithPagination(Long idUser, int first, int max) {	

		List<FriendsEntity> entities = null;

		try{

			String hql = " FROM FriendsEntity o where o.userEntity.id=:pIdUser ORDER BY o.id ASC";

			TypedQuery<FriendsEntity> query = this.em.createQuery(hql, FriendsEntity.class);

			query.setParameter("pIdUser", idUser);
			query.setFirstResult(first);
			query.setMaxResults(max);
			
			entities = query.getResultList();

		}catch(NoResultException e){
			entities = new ArrayList<FriendsEntity>();
		}

		return entities;
	}


}
