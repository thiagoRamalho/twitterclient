package br.com.trama.twitterclient.core;

import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import br.com.trama.twitterclient.util.DbConstants;
import br.com.trama.twitterclient.util.FileUtil;


public abstract class JPAUtil {

	private static EntityManagerFactory factory;
	private static EntityManager em;
	private static String finalPU;

	static{

		Properties propertiesConfig = FileUtil.getPropertiesConfig(DbConstants.CURRENT_PU_PROP);

		finalPU = propertiesConfig.getProperty(DbConstants.PU_KEY);
		
		factory = Persistence.createEntityManagerFactory(finalPU, propertiesConfig);
	}

	public static EntityManager getEM(){

		em = factory.createEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();

		return em;
	}
	
	public static EntityManager getNewEM(){
		return factory.createEntityManager();
	}




	public static void close(){

		if(em != null && em.isOpen()){
			em.getTransaction().commit();
			em.close();
		}
	}
	
	public static String getFinalPU() {
		return finalPU;
	}
}
