package br.com.trama.twitterclient.core.load.twitter;

import java.util.Calendar;
import java.util.List;

import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import br.com.trama.twitterclient.core.StatusProcess;
import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.model.dao.FriendsEntityDAO;
import br.com.trama.twitterclient.model.dao.UserEntityDAO;
import br.com.trama.twitterclient.model.entity.FriendsEntity;
import br.com.trama.twitterclient.model.entity.StatusSearch;
import br.com.trama.twitterclient.model.entity.UserEntity;

public class LoadFriendsProcess extends TwitterProcessTemplate{

	private UserEntityDAO userEntityDAO;

	private FriendsEntityDAO friendsEntityDAO;

	private StringBuilder builder;

	public LoadFriendsProcess(UserEntityDAO userEntityDAO, FriendsEntityDAO friendsEntityDAO, Twitter twitter) {
		super(twitter);
		this.userEntityDAO = userEntityDAO;
		this.friendsEntityDAO = friendsEntityDAO;
	}

	@Override
	public StatusProcess execute() {

		StatusProcess statusProcess = StatusProcess.FOLLOWERS;

		publish("iniciando pesquisa de Amigos");

		try{

			List<UserEntity> users = userEntityDAO.findWithStatusFriends(StatusSearch.NOT_EXECUTED, StatusSearch.PARCIAL);

			for (UserEntity userEntity : users) {
				
				logAppend("buscando Amigos: "+userEntity.getScreenName());

				int responseStatus = searchAndSave(userEntity);

				if(isRateLimiteStatus(responseStatus)){

					logAppend("Limite de requisições excedido");

					publish(builder.toString());
					return StatusProcess.WAIT;
				}

				long followersCount = this.friendsEntityDAO.countByUser(userEntity);

				logAppend("Total de: ["+followersCount+"] amigos gravados para: "+userEntity.getScreenName());

				publish(builder.toString());

				builder = null;
			}

		}catch(Exception e){
			statusProcess = StatusProcess.ERROR;
			logAppend(e.getMessage());
			publish(builder.toString());
		}
		finally{
			builder = null;
		}

		return statusProcess;
	}

	private void logAppend(String msg) {

		if(builder == null){
			builder = new StringBuilder();
		}

		builder.append(TwitterConstants.LINE_SEPARATOR).append(msg);
	}

	private int searchAndSave(UserEntity userEntity) throws TwitterException {

		int responseStatusCode = TwitterConstants.STATUS_CODE_OK;

		long cursor = userEntity.getLastCursorFollowers();

		try{

			cursor = cursor > -1 ? cursor : -1;

			PagableResponseList<User> friendsList;

			logAppend("last cursor ["+cursor+"]");

			if(!StatusSearch.COMPLETE.equals(userEntity.getStatusSearchFriends())){

				do{

					boolean rateRemaining = this.isRateRemaining(TwitterConstants.RESOURCE_FAMILY_FRIENDS, 
							TwitterConstants.RESOURCE_KEY_FRIENDS_LIST);

					if(cursor != 0 && !rateRemaining){
						return TwitterConstants.STATUS_CODE_RATE_LIMITED_v1_1;
					}

					friendsList = twitter.getFriendsList(userEntity.getScreenName(), cursor, TwitterConstants.COUNT_LIMIT);
					
					cursor = friendsList.getNextCursor();

					userEntity.setLastCursorFriends(cursor);
					userEntity.setStatusSearchFriends(StatusSearch.PARCIAL);
					
					StatusProcess statusProcess = saveFriends(userEntity, friendsList);

					//se achou algum resultado significa que ja recuperou todos os 
					//registros, para o processo portanto
					if(StatusProcess.NEXT_USER.equals(statusProcess)){
						
						userEntity.setStatusSearchFriends(StatusSearch.COMPLETE);
						userEntity.setLastCursorFriends(0);
						
						return TwitterConstants.STATUS_CODE_OK;
					}
					
				} while (cursor != 0);

				userEntity.setLastCursorFriends(0);
				userEntity.setStatusSearchFriends(StatusSearch.COMPLETE);
			} 

		}catch (TwitterException e) {

			responseStatusCode = e.getStatusCode();

			userEntity.setStatusSearchFriends(StatusSearch.PARCIAL);
			userEntity.setLastCursorFriends(cursor);
			
			logAppend(String.format(TwitterConstants.ERROR_TWITTER_MSG, e.getErrorCode(), e.getStatusCode(), e.getErrorMessage()));

			if(e.getStatusCode() == TwitterConstants.STATUS_CODE_NOT_FOUND){
				userEntity.setStatusSearchFriends(StatusSearch.NOT_FOUND);
			}
		}
		finally{
			this.userEntityDAO.save(userEntity);
		}

		return responseStatusCode;
	}

	private StatusProcess saveFriends(UserEntity userEntity, PagableResponseList<User> friendsList) {

		for (User user : friendsList) {

			List<FriendsEntity> entities = this.friendsEntityDAO.findByScreenName(user.getScreenName());

			if(!entities.isEmpty()){
				return StatusProcess.NEXT_USER;
			}
			
			FriendsEntity friendsEntity = new FriendsEntity();
			friendsEntity.setDate(Calendar.getInstance());
			friendsEntity.setScreenName(user.getScreenName());
			friendsEntity.setUserEntity(userEntity);
			
			friendsEntityDAO.save(friendsEntity);
		}
		
		return StatusProcess.CONTINUE_PROCESS;
	}
}
