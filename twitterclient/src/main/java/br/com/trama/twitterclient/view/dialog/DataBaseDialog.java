package br.com.trama.twitterclient.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import br.com.trama.twitterclient.model.entity.DatabaseConfigEntity;
import br.com.trama.twitterclient.util.DbConstants;
import br.com.trama.twitterclient.util.FileUtil;

public class DataBaseDialog extends JDialog {

	
	private static final long serialVersionUID = 1L;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldUrlConnection;
	private JTextField textFieldLogin;
	private JTextField textFieldPassword;
	
	private JComboBox<DatabaseConfigEntity> comboBoxDatabase;

	/**
	 * Create the dialog.
	 * @param frame 
	 */
	public DataBaseDialog(Frame frame) {
		
		super(frame);
		setModal(true);
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(6, 6, 438, 227);
		panel.setLayout(null);
		panel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

		contentPanel.add(panel);
		
		JLabel lblBanco = new JLabel("SGDB:");
		lblBanco.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblBanco.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBanco.setBounds(6, 22, 92, 16);
		panel.add(lblBanco);
		
		textFieldUrlConnection = new JTextField();
		textFieldUrlConnection.setBounds(110, 50, 322, 28);
		panel.add(textFieldUrlConnection);
		textFieldUrlConnection.setColumns(10);
		
		JLabel lblUser = new JLabel("Login:");
		lblUser.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblUser.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUser.setBounds(6, 88, 92, 16);
		panel.add(lblUser);
		
		textFieldLogin = new JTextField();
		textFieldLogin.setBounds(110, 82, 322, 28);
		panel.add(textFieldLogin);
		textFieldLogin.setColumns(10);
		
		JLabel lblPassword = new JLabel("Senha:");
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPassword.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblPassword.setBounds(6, 118, 92, 16);
		panel.add(lblPassword);
		
		textFieldPassword = new JTextField();
		textFieldPassword.setBounds(110, 112, 322, 28);
		panel.add(textFieldPassword);
		textFieldPassword.setColumns(10);
		
		comboBoxDatabase = new JComboBox<DatabaseConfigEntity>();
		comboBoxDatabase.setBounds(110, 18, 322, 27);
		panel.add(comboBoxDatabase);
		
		JLabel lblPathDataBase = new JLabel("URL Conexão:");
		lblPathDataBase.setBounds(6, 54, 92, 16);

		panel.add(lblPathDataBase);
		lblPathDataBase.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBounds(6, 235, 438, 37);
		buttonPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

		contentPanel.add(buttonPanel);
		buttonPanel.setLayout(null);
		
		JButton btnSave = new JButton("Salvar");
		btnSave.setBounds(216, 6, 102, 29);
		btnSave.addActionListener(new SaveListener());
		buttonPanel.add(btnSave);
		
		JButton btnExit = new JButton("Cancelar");
		btnExit.setBounds(330, 6, 102, 29);
		btnExit.addActionListener(new CloseListener());
		buttonPanel.add(btnExit);
		
		loadDBs();
		
		setConfigsDB(DbConstants.CURRENT_PU_PROP);
		
		comboBoxDatabase.addActionListener(new SelectionSGDBListener());
	}
	
	private void loadDBs() {
		Properties propertiesConfig = FileUtil.getPropertiesConfig(DbConstants.PROP_DBS);
		
		Set<Entry<Object,Object>> entrySet = propertiesConfig.entrySet();
		
		for (Entry<Object, Object> entry : entrySet) {
			comboBoxDatabase.addItem(new DatabaseConfigEntity(""+entry.getKey(), ""+entry.getValue()));
		}
	}

	private void setConfigsDB(String prop){
		
		Properties propertiesConfig = FileUtil.getPropertiesConfig(prop);
		
		String puKey = (String) propertiesConfig.get(DbConstants.PU_KEY);
		
		boolean isEnable = !DbConstants.HSQLDB_PU.equals(puKey);
		
		textFieldUrlConnection.setText(propertiesConfig.getProperty(DbConstants.URL));
		textFieldUrlConnection.setEnabled(isEnable);
		
		textFieldLogin.setText(propertiesConfig.getProperty(DbConstants.USER));
		textFieldLogin.setEnabled(isEnable);

		textFieldPassword.setText(propertiesConfig.getProperty(DbConstants.PASS));
		textFieldPassword.setEnabled(isEnable);
		
		comboBoxDatabase.setSelectedItem(new DatabaseConfigEntity(puKey, prop));
	}
	
	class SelectionSGDBListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			DatabaseConfigEntity selectedItem = (DatabaseConfigEntity) comboBoxDatabase.getSelectedItem();
			setConfigsDB(selectedItem.getFileName());
		}
	}
	
	class SaveListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			DatabaseConfigEntity comboValue = (DatabaseConfigEntity) comboBoxDatabase.getSelectedItem();
			
			Properties propertiesConfig = FileUtil.getPropertiesConfig(DbConstants.CURRENT_PU_PROP);
			propertiesConfig.put(DbConstants.PU_KEY, comboValue.getPuKey());
			propertiesConfig.put(DbConstants.URL, textFieldUrlConnection.getText());
			propertiesConfig.put(DbConstants.USER, textFieldLogin.getText());
			propertiesConfig.put(DbConstants.PASS, textFieldPassword.getText());
			
			FileUtil.saveProp(propertiesConfig, DbConstants.CURRENT_PU_PROP);
			
			JOptionPane.showMessageDialog(DataBaseDialog.this, "Reinicie a aplicação para que as\n alterações surtam efeito!", "Atenção", JOptionPane.INFORMATION_MESSAGE);
			
			DataBaseDialog.this.dispose();
		}
	}
	
	class CloseListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			DataBaseDialog.this.dispose();
		}
	}
}
