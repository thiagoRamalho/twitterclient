package br.com.trama.twitterclient.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import br.com.trama.twitterclient.core.TwitterConstants;

public abstract class FileUtil {

	public static Properties getPropertiesConfig(String propName) {

		Properties props = new Properties();
		InputStream is = null;

		// First try loading from the current directory
		try {
			File f = new File(propName);
			is = new FileInputStream( f );
		}
		catch ( Exception e ) { is = null; }

		try {
			if ( is == null ) {
				// Try loading from classpath
				is = FileUtil.class.getResourceAsStream(propName);
			}

			// Try loading properties from the file (if found)
			props.load( is );
		}
		catch ( Exception e ) { 
			e.printStackTrace();
		}
		finally{
			try {
				is.close();
			} catch (IOException e) {
			}
		}

		return props;
	}

	public static void saveProp(Properties propertiesConfig, String pathProp) {
		
		try {
	        File f = new File(pathProp);
	        OutputStream out = new FileOutputStream( f );
	        SimpleDateFormat dateFormat = TwitterConstants.DATE_FORMAT;
	        propertiesConfig.store(out, "Updated at: "+dateFormat.format(Calendar.getInstance().getTime()));
	        out.close();
	    }
	    catch (Exception e ) {
	        e.printStackTrace();
	    }
		
	}
}
