package br.com.trama.twitterclient.core.csv.write;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import br.com.trama.twitterclient.core.JPAUtil;
import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.model.dao.FollowerEntityDAO;
import br.com.trama.twitterclient.model.dao.UserEntityDAO;
import br.com.trama.twitterclient.model.entity.FollowerEntity;
import br.com.trama.twitterclient.model.entity.UserEntity;

public class WriteDataFriendsUserCSVFileProcess extends WriteCSVFileProcess{

	private File selectedFile;
	private Writer out;

	public WriteDataFriendsUserCSVFileProcess(File selectedFile) {
		this.selectedFile = selectedFile;
	}

	@Override
	protected void doProcess() {

		List<UserEntity> findAll = getUsers();

		int total = 0;

		if(findAll.isEmpty() || !createOutPutStream()){
			super.observerProcess.update("Processo Finalizado, nenhum registro foi gravado");
			return;
		}

		for (UserEntity userEntity : findAll) {

			List<FollowerEntity> followers = getFollowersBy(userEntity);

			for (FollowerEntity followerEntity : followers) {

				StringBuilder builder = new StringBuilder();

				builder.append(userEntity.getScreenName());
				builder.append(TwitterConstants.COMMA);
				builder.append(followerEntity.getScreenName());
				builder.append(TwitterConstants.COMMA);
				builder.append(TwitterConstants.LINE_SEPARATOR);

				writeLine(builder);
			}

			total+=followers.size();

			super.observerProcess.update("Gravadas ["+followers.size()+"] linhas para Usuário ["+userEntity.getScreenName()+"]");
		}

		closeOutputStream();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		
		super.observerProcess.update(TwitterConstants.LINE_SEPARATOR + "Gravadas ["+total+"] linhas no arquivo ["+selectedFile.getPath()+"]");
	}

	private boolean createOutPutStream() {

		boolean isOk = true;

		try {

			this.selectedFile.delete();
			out = new BufferedWriter(new FileWriter(this.selectedFile));

		} catch (Exception e) {
			isOk = false;
			super.observerProcess.update("Error create file: "+ 
					(this.selectedFile != null ? this.selectedFile.getPath() : null));
		}

		return isOk;
	}

	private void closeOutputStream(){
		try {

			if(out != null){
				out.flush();
				out.close();
			}

		} catch (IOException e) {
			super.observerProcess.update("Erro ao fechar arquivo ["+selectedFile.getPath()+"]");
		}
	}

	private void writeLine(StringBuilder builder) {

		try {
			out.append(builder.toString());
		}
		catch(IOException e){
			super.observerProcess.update("Error write: "+builder.toString());
		}
	}

	private List<FollowerEntity> getFollowersBy(UserEntity userEntity) {
		
		FollowerEntityDAO followerEntityDAO = new FollowerEntityDAO(JPAUtil.getEM());
		
		List<FollowerEntity> followers = followerEntityDAO.findByIdUser(userEntity.getId());
		
		JPAUtil.close();
		
		return followers;
	}

	private List<UserEntity> getUsers() {
		
		UserEntityDAO userEntityDAO = new UserEntityDAO(JPAUtil.getEM());

		List<UserEntity> findAll = userEntityDAO.findAll();

		JPAUtil.close();
		
		return findAll;
	}
}
