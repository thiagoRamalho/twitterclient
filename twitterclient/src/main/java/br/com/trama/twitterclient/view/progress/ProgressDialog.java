package br.com.trama.twitterclient.view.progress;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import br.com.trama.twitterclient.core.SubjectProcess;
import br.com.trama.twitterclient.core.TwitterConstants;
import br.com.trama.twitterclient.view.ObserverProcess;

public class ProgressDialog extends JDialog implements PropertyChangeListener {

	private static final long serialVersionUID = 1L;

	private JTextArea textAreaLog;

	private JButton btnStop;

	private TaskSwingWorker taskSwingWorker;

	/**
	 * Create the frame.
	 * @param owner 
	 */
	public ProgressDialog(Frame owner, SubjectProcess subjectProcess, String processDescription) {
		super(owner);
		
		this.taskSwingWorker = new TaskSwingWorker(subjectProcess);
		
		setModal(true);
		setBounds(100, 100, 300, 220);
		setResizable(false);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		
		addWindowListener(new CloseAdapter());
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panelHeader = new JPanel();
		panelHeader.setBounds(6, 10, 288, 28);
		contentPane.add(panelHeader);
		panelHeader.setLayout(null);
		panelHeader.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		
		JLabel lblProcessDescription = new JLabel("Descrição:");
		lblProcessDescription.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblProcessDescription.setBounds(6, 6, 80, 16);
		panelHeader.add(lblProcessDescription);
		
		JLabel lblProgressDescriptionText = new JLabel(processDescription);
		lblProgressDescriptionText.setBounds(98, 6, 184, 16);
		panelHeader.add(lblProgressDescriptionText);
		
		textAreaLog = new JTextArea();
		textAreaLog.setTabSize(3000);
		textAreaLog.setEditable(false);
		textAreaLog.setRows(5);
		textAreaLog.setColumns(20);
		
		JScrollPane scrollPane = new JScrollPane(textAreaLog);
		scrollPane.setBounds(6, 50, 288, 91);
		contentPane.add(scrollPane);
		
		JPanel panel = new JPanel();
		panel.setBounds(6, 153, 288, 39);
		contentPane.add(panel);
		panel.setLayout(null);
		panel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		
		btnStop = new JButton("Sair");
		btnStop.addActionListener(new StopListener());
		btnStop.setBounds(165, 6, 117, 29);
		btnStop.setEnabled(false);
		panel.add(btnStop);

		this.setLocationRelativeTo(null);
	}

	public void start(){
		taskSwingWorker.addPropertyChangeListener(this);
		taskSwingWorker.execute();
	}
	
	private void confirmMessage(){
		
		if(taskSwingWorker.isDone()){
			this.dispose();
			return;
		}
		
		int returnOption = 
		JOptionPane.showConfirmDialog(ProgressDialog.this, "Finalizar Processo", 
				"Deseja Realmente Finalizar Processo?", JOptionPane.OK_CANCEL_OPTION);
		
		if(returnOption == JOptionPane.YES_OPTION){
			btnStop.setEnabled(true);
			taskSwingWorker.cancel(true);
			this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		}
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {        

		if (TwitterConstants.PROGRESS.equals(evt.getPropertyName())) {
			//progressBar.setValue(progress);
			textAreaLog.append(String.format(
					"Completed %d%% of task.\n", evt.getNewValue()));
		} 
		else if(TwitterConstants.MESSAGE.equals(evt.getPropertyName())){
			textAreaLog.append(String.format("%s \n", evt.getNewValue()));
		}
	}
	
	
	class TaskSwingWorker extends SwingWorker<Void, String> implements ObserverProcess{
		
		private SubjectProcess subjectProcess;
		
		public TaskSwingWorker(SubjectProcess subjectProcess) {
			super();
			this.subjectProcess = subjectProcess;
			this.subjectProcess.setObserverProcess(this);
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			
			publish("Processo Iniciado...");
			
			subjectProcess.execute();
			
			return null;
		}
		
		@Override
		protected void done() {
			super.done();
			
			if(isCancelled()){
				publish("Processo Cancelado");
			}
			else {
				publish("Processo Finalizado!");
			}
			
			ProgressDialog.this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			btnStop.setEnabled(true);
		}
		
		@Override
		protected void process(List<String> chunks) {
			super.process(chunks);
	        firePropertyChange(TwitterConstants.MESSAGE,null,chunks.get(0));
		}

		@Override
		public void update(String string) {
			publish(string);
		}
	}
	
	class CloseAdapter extends WindowAdapter{
		
		@Override
		public void windowClosed(WindowEvent e) {
			confirmMessage();
		}
	}
	
	class StopListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			confirmMessage();
		}

	}
}
