package br.com.trama.twitterclient.model.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.trama.twitterclient.model.entity.StatusSearch;
import br.com.trama.twitterclient.model.entity.UserEntity;

public class UserEntityDAO {

	private EntityManager em;
	
	public UserEntityDAO(EntityManager em) {
		super();
		this.em = em;
	}

	public List<UserEntity> findAll(){
		
		String hql = " from UserEntity p ORDER BY p.id ASC ";

		TypedQuery<UserEntity> query = this.em.createQuery(hql, UserEntity.class);

		return query.getResultList();
	}

	public void deleteAll() {

		String hql = " delete from UserEntity ";

		Query query = this.em.createQuery(hql);

		query.executeUpdate();
	}

	public void save(UserEntity userEntity) {
		em.merge(userEntity);
	}

	public List<UserEntity> findByScreenName(String name) {
		
		List<UserEntity> users = null;
		
		try{
			String hql = " from UserEntity p WHERE UPPER(p.screenName) = :name";

			TypedQuery<UserEntity> query = this.em.createQuery(hql, UserEntity.class);

			query.setParameter("name", name.trim().toUpperCase());

			users = query.getResultList();

		}catch(NoResultException e){
			users = new ArrayList<UserEntity>();
		}
		
		return users;
	}

	public List<UserEntity> findWithStatusFollower(StatusSearch... status) {
		
		List<UserEntity> users = new ArrayList<UserEntity>();
		
		try{
			String hql = " from UserEntity p WHERE UPPER(p.statusSearchFollowers) IN :status ORDER BY p.lastCursorFollowers ASC";

			TypedQuery<UserEntity> query = this.em.createQuery(hql, UserEntity.class);

			query.setParameter("status", Arrays.asList(status));

			users = query.getResultList();

		}catch(NoResultException e){
			users = new ArrayList<UserEntity>();
		}
		
		return users;
	}

	public List<UserEntity> findWithStatusFriends(StatusSearch... status) {
		
		List<UserEntity> users = new ArrayList<UserEntity>();
		
		try{
			String hql = " from UserEntity p WHERE UPPER(p.statusSearchFriends) IN :status "
					   + "ORDER BY p.lastCursorFriends ASC";

			TypedQuery<UserEntity> query = this.em.createQuery(hql, UserEntity.class);

			query.setParameter("status", Arrays.asList(status));

			users = query.getResultList();

		}catch(NoResultException e){
			users = new ArrayList<UserEntity>();
		}
		
		return users;
	}

	public List<UserEntity> findWithPagination(int first, int max) {
		
		List<UserEntity> users = new ArrayList<UserEntity>();
		
		try{
			
			String hql = " from UserEntity p ORDER BY p.id ASC ";

			TypedQuery<UserEntity> query = this.em.createQuery(hql, UserEntity.class);

			query.setFirstResult(first);
			
			query.setMaxResults(max);
			
			users = query.getResultList();

		}catch(NoResultException e){
			users = new ArrayList<UserEntity>();
		}
		
		return users;
	}
	
	public long count() {

		String jpql = "SELECT count(*) FROM UserEntity";  
		
		Query query = this.em.createQuery(jpql);
		
		return (Long) query.getSingleResult();
	}

	public int updateStatusFollowersAndFriendsWhereComplete(StatusSearch statusSearch) {

		String jpql = "UPDATE UserEntity u SET u.statusSearchFollowers =:pStatus1, u.statusSearchFriends =:pStatus2 ";
		jpql+= "WHERE u.statusSearchFollowers =:pNowStatus OR u.statusSearchFriends =:pNowStatus";
		
		Query query = em.createQuery(jpql);
		query.setParameter("pStatus1", statusSearch);
		query.setParameter("pStatus2", statusSearch);
		query.setParameter("pNowStatus", StatusSearch.COMPLETE);
		
		int executeUpdate = query.executeUpdate();
		
		return executeUpdate;
	}

}
