package br.com.trama.twitterclient.core;

import java.text.SimpleDateFormat;

public abstract class TwitterConstants {

	public static final int COUNT_LIMIT = 200;
	public static final String RESOURCE_FAMILY_FOLLOWERS = "followers";
	public static final String RESOURCE_KEY_FOLLOWERS_LIST = "/followers/list";
	
	public static final String RESOURCE_FAMILY_FRIENDS = "friends";
	public static final String RESOURCE_KEY_FRIENDS_LIST = "/friends/list";
	
	public static final int STATUS_CODE_OK = 200;
	public static final int STATUS_CODE_NOT_FOUND = 404;
	public static final int STATUS_CODE_RATE_LIMITED_v1_0 = 420;
	public static final int STATUS_CODE_RATE_LIMITED_v1_1 = 429;
	public static final int MINIMUM_TIME_IN_SECONDS = 60;
	
	public static final String PROGRESS = "progress";
	public static final String MESSAGE = "message";
	public static final Object LINE_SEPARATOR = System.getProperty("line.separator");
	
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	
	public static final int MIN_ROW = 12;

	public static final String ERROR_TWITTER_MSG = "Twitter Status Code: [ %s ] Twitter Error Status Code:  [ %s ]  msg:  [ %s ] ";
	public static final Object COMMA = ";";
}
