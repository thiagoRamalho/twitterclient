package br.com.trama.twitterclient.core.csv.read;

import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

import br.com.trama.twitterclient.core.StatusProcess;
import br.com.trama.twitterclient.core.SubjectProcess;
import br.com.trama.twitterclient.view.ObserverProcess;

public abstract class ReadCSVFileProcess implements SubjectProcess{

	private File file;
	protected ObserverProcess observerProcess;
	
	protected long gravados;
	
	protected long ignorados;
	
	protected long processados;

	public ReadCSVFileProcess(File file) {
		super();
		this.file = file;
	}

	@Override
	public StatusProcess execute(){
	
		Scanner scanner = null;
		
		try{

			scanner = new Scanner(new FileReader(file));
			scanner.useDelimiter("\\||\\n");

			while(scanner.hasNext()){
				
				doProcess(scanner.next());

				if(observerProcess.isCancelled()){
					observerProcess.update("== Processo Cancelado == ");
					return StatusProcess.FINISH;
				}
			}

		}catch(Exception e){
			observerProcess.update("** Erro ao ler Arquivo [ "+e.getMessage()+" ] ");
		}finally{
			
			StringBuilder builder = new StringBuilder();
			builder.append("        Total: "+processados)
			.append("\n"+" Gravados: "+gravados)
			.append("\n"+"Ignorados: "+ignorados);
			
			observerProcess.update(builder.toString());
			
			if(scanner != null){
				scanner.close();
			}
		}
		
		return StatusProcess.FINISH;
	}
	
	protected abstract void doProcess(String line);

	@Override
	public void setObserverProcess(ObserverProcess observerProcess) {
		this.observerProcess = observerProcess;
	}
}
