package br.com.trama.twitterclient.util;

public class DbConstants {

	public static final String HSQLDB_PU = "HSQLDB";
	
	public static final String PU_KEY ="pu";

	public static final String URL = "javax.persistence.jdbc.url";
	
	public static final String USER = "javax.persistence.jdbc.user";
	
	public static final String PASS = "javax.persistence.jdbc.password";

	public static final String CURRENT_PU_PROP = "current_pu.properties";

	public static final String PROP_DBS = "data_base.properties";
}
