package br.com.trama.twitterclient.core.csv.read;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import br.com.trama.twitterclient.core.JPAUtil;
import br.com.trama.twitterclient.model.dao.UserEntityDAO;
import br.com.trama.twitterclient.model.entity.StatusSearch;
import br.com.trama.twitterclient.model.entity.UserEntity;

public class ReadUsersCSVProcess extends ReadCSVFileProcess{

	private UserEntityDAO userEntityDAO;

	public ReadUsersCSVProcess(File file) {
		super(file);
	}

	@Override
	protected void doProcess(String line) {

		try{

			userEntityDAO = new UserEntityDAO(JPAUtil.getEM());

			line = line.trim();

			String[] split = line.split(";");

			for (String screenName : split) {

				if(screenName != null && !screenName.trim().isEmpty()){

					UserEntity userEntity = new UserEntity();
					userEntity.setDate(Calendar.getInstance());
					userEntity.setScreenName(screenName.trim());
					userEntity.setStatusSearchFollowers(StatusSearch.NOT_EXECUTED);
					userEntity.setStatusSearchFriends(StatusSearch.NOT_EXECUTED);

					if(userEntity.getScreenName().startsWith("@")){
						userEntity.setScreenName(userEntity.getScreenName().substring(1));
					}
					
					List<UserEntity> temp = this.userEntityDAO.findByScreenName(userEntity.getScreenName());

					if(temp.isEmpty()){
						this.userEntityDAO.save(userEntity);
						gravados++;
					} else {
						ignorados++;
					}
					processados++;
				}

				if(observerProcess.isCancelled()){
					return;
				}
			}

		}finally{
			JPAUtil.close();
		}
	}
}
